package org.batsunov;


public class Pojo {

    @NotNull
    public String name;


    public Pojo() {
    }

    public Pojo(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Pojo{" +
                "name='" + name + '\'' +
                '}';
    }
}
