package org.batsunov;

import java.lang.reflect.Field;



public class NotNullValidator implements Validator<NotNull> {


    @Override
    public void validate(Object obj) throws ValidationException, IllegalAccessException {


        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(getAnnotationType())) {
                field.setAccessible(true);

                if (field.get(obj) == null) {

                    throw new ValidationException(field.getAnnotation(getAnnotationType()).message());
                }
            }
        }

    }

    @Override
    public Class<NotNull> getAnnotationType() {
        return NotNull.class;
    }
}
