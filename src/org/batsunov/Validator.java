package org.batsunov;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;

public interface Validator<T extends Annotation> {

   void validate(Object o) throws ValidationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException;

    Class<T> getAnnotationType();

}
