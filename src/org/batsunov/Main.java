package org.batsunov;


public class Main {

    public static void main(String[] args) throws Exception {

        Main m = new Main();
        Pojo pojo = new Pojo();

        Object print = MethodRunner.invoke(Main.class, m, "print", new Class<?>[]{Pojo.class}, pojo);

    }

    public void print(@Valid Pojo p) {
        System.out.println(p);
    }
}

