package org.batsunov;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Optional;


public class MethodRunner {
    /**
     * @param clazz      класс объекта на котором нужно вызвать метод
     * @param obj        обьект класса, или null если метод статический
     * @param methodName имя метода
     * @param paramTypes массив типов параметров метода
     * @param params     сами параметры
     * @return возвращаемое методом значение
     */
    public static Object invoke(Class<?> clazz, Object obj, String methodName, Class<?>[] paramTypes, Object... params) throws Exception {


        Method method = clazz.getDeclaredMethod(methodName, paramTypes);
        method.setAccessible(true);




         for (Parameter parameter : method.getParameters()) {
             if (parameter.isAnnotationPresent(Valid.class)) {
                 NotNullValidator validator = new NotNullValidator();
                 System.out.println("Создали обьект валид");

                 for (int i = 0; i < params.length; i++) {
                     if (parameter.getType().isInstance(params[i])) {
                         validator.validate(params[i]);
                     }
                 }
             }
         }




        return method.invoke(obj, params);
    }
}
